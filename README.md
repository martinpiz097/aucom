# AuCom (AudioCommunication)
Java library that provides tools for audio recording and playback.

## Note:
	You must download the ByteBuffer project in order to compile if you want to make changes. The project can be found at the following 
	address: https://gitlab.com/martinpiz097/ByteBuffer
